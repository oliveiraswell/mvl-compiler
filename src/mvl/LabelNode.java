package mvl;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LabelNode {

    private int number;
    private String name;
    private boolean checked;
    private int line;
    
    public LabelNode(int number, String name, boolean checked, int line) {
        this.number = number;
        this.name = name;
        this.checked = checked;
        this.line = line;
    }

}
