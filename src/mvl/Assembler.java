package mvl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

class Assembler {

    private String input;
    private int line;
    private StringCharacterIterator inputIt;
    private Token actualToken;
    private boolean error;
    public String outPutErrors;
    public List<InstructionTerm> FinalTable = new ArrayList<>();
    public List<LabelNode> LabelTable = new ArrayList<>();

    public Assembler(String in) throws FileNotFoundException, IOException {

        outPutErrors = "";
        line = 1;
        System.out.println("Entrada: " + in);
        File inputFile = new File(in);
        FileReader fr = new FileReader(inputFile);
        int size = (int) inputFile.length();
        char[] buffer = new char[size];
        fr.read(buffer, 0, size);
        input = new String(buffer);
        inputIt = new StringCharacterIterator(input);
    }

    public boolean isError() {
        return error;
    }

    public void printVector() {
        int a;
        System.out.println(FinalTable.size());
        for (int i = 0; i < FinalTable.size(); i++) {
            a = i + 1;
            System.out.print(a + " " + (FinalTable.get(i)).getTermA().value);
            if (FinalTable.get(i).getSize() > 1) {
                System.out.print(" " + (FinalTable.get(i)).getTermB().value);
                if (FinalTable.get(i).getSize() > 2) {
                    System.out.print(" " + FinalTable.get(i).getTermC().value + " ");
                    if (FinalTable.get(i).getSize() == 4) {
                        System.out.print(" " + FinalTable.get(i).getTermD().value + " ");
                    }
                }
            }
            System.out.print("\n");
        }
    }

    public void printLabelVector() {
        int a = 0;
        for (int i = 0; i < LabelTable.size(); i++) {
            System.out.println(LabelTable.get(i).getName() + " " + LabelTable.get(i).getNumber() + " " + LabelTable.get(i).isChecked());
        }
    }

    public int containsLabelInVector(String inString) {
        for (int i = 0; i < LabelTable.size(); i++) {
            if (inString.equals(LabelTable.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }

    public int verifyLabelTable() {
        int auxIndex;
        InstructionTerm actualLine = new InstructionTerm();

        for (int i = 0; i < FinalTable.size(); i++) {
            if (FinalTable.get(i).getSize() > 0) {
                System.out.println("Tabela em " + i + " " + FinalTable.get(i).getTermA().value);
                if ("JUMP".equals(FinalTable.get(i).getTermA().value.toUpperCase())) {
                    if (FinalTable.get(i).getSize() == 3 && (FinalTable.get(i).getTermC().name == EnumToken.ID)) {
                        auxIndex = this.containsLabelInVector(FinalTable.get(i).getTermC().value);
                        if (auxIndex < 0) {
                            this.outPutErrors += "Erro encontrado, Label: " + FinalTable.get(i).getTermC().value + " nao encontrado." + '\n';
                            return 1;
                        } else {
                            actualLine.setTermA(FinalTable.get(i).getTermA());
                            actualLine.setTermB(FinalTable.get(i).getTermB());
                            actualLine.setTermC(new Token(EnumToken.NUMBER, String.valueOf(LabelTable.get(auxIndex).getNumber() + 14)));
                            FinalTable.set(i, new InstructionTerm(actualLine));
                        }
                    }
                }
            } else {
                return -1;
            }
        }
        return -1;
    }

    public Token nextTerm() {
        Token tok = new Token(EnumToken.UNDEF);
        int begin, end;
        String lexema;

        while (Character.isWhitespace(inputIt.current())
                || (inputIt.current() == '-' && (input.charAt(inputIt.getIndex() + 1) == '-'))) {
            while (Character.isWhitespace(inputIt.current())) {
                if (inputIt.current() == '\n') {
                    line++;
                }
                inputIt.next();
            }

            if (inputIt.current() == '-' && input.charAt(inputIt.getIndex() + 1) == '-') {
                inputIt.next();
                inputIt.next();
                while (inputIt.current() != '\n') {
                    if (inputIt.getIndex() >= inputIt.getEndIndex()) {
                        tok.name = EnumToken.EOF;
                        return tok;
                    }
                    inputIt.next();
                }
                line++;
                inputIt.next();
            }
            while (inputIt.current() == 0) {
                inputIt.next();
            }
        }

        while (inputIt.current() == 0) {
            inputIt.next();
        }

        if (inputIt.getIndex() >= inputIt.getEndIndex()) {
            tok.name = EnumToken.EOF;
            return tok;
        }

        if (input.charAt(inputIt.getIndex()) == ',') {
            inputIt.next();
            tok.name = EnumToken.COMMA;
            return tok;
        }

        begin = inputIt.getIndex();
        end = inputIt.getIndex();
        lexema = new String(input.substring(begin, end + 1));
        tok.value = lexema;

        if (Character.isDigit(inputIt.current())
                || (Character.isDigit(input.charAt(inputIt.getIndex() + 1))
                && inputIt.current() == '-')) {

            int signal = 1;

            if (inputIt.current() == '-') {
                signal = -1;
                inputIt.next();
            }
            tok.name = EnumToken.NUMBER;

            while (Character.isDigit(inputIt.current())) {
                if (inputIt.current() == '\n') {
                    line++;
                }
                inputIt.next();
            }

            end = inputIt.getIndex();
            lexema = new String(input.substring(begin, end));
            tok.attribute = EnumToken.INTEGER;
            tok.value = lexema;
            return tok;
        }//Fim NUMBER
        if (Character.isLetter(inputIt.current())
                || inputIt.current() == '_'
                || inputIt.current() == '$') {
            inputIt.next();
            while ((Character.isLetter(inputIt.current())
                    || Character.isDigit(inputIt.current())
                    || inputIt.current() == '_')
                    && inputIt.current() != ':') {
                inputIt.next();
            }
            tok.name = EnumToken.ID;
            if (inputIt.current() == ':') {
                tok.name = EnumToken.LABEL;
            }
            end = inputIt.getIndex();
            lexema = new String(input.substring(begin, end));
            tok.value = lexema;
            lexema = lexema.toUpperCase();
            if ("STORE".equals(lexema)
                    || "JUMP".equals(lexema)
                    || "ADDI".equals(lexema)
                    || "NOP".equals(lexema)
                    || "LOAD".equals(lexema)
                    || "MOV".equals(lexema)
                    || "PUSH".equals(lexema)
                    || "POP".equals(lexema)) {
                tok.name = EnumToken.INSTRUCTIONI;
            } else if ("ADD".equals(lexema)
                    || "SUBT".equals(lexema)
                    || "MULT".equals(lexema)
                    || "DIV".equals(lexema)
                    || "GT".equals(lexema)
                    || "LS".equals(lexema)) {
                tok.name = EnumToken.INSTRUCTION;
            } else if (lexema.charAt(0) == '$'
                    && Character.isDigit(lexema.charAt(1))) {
                if (lexema.length() <= 3) {
                    lexema = lexema.charAt(1) + (lexema.length() == 3 ? (lexema.charAt(2) + "") : "");
                    tok.value = lexema;
                    tok.name = EnumToken.REGISTER;
                }
            }
            return tok;
        }
        inputIt.next();
        return tok;
    }

    public void read() throws java.lang.Exception {
        InstructionTerm actualLine = new InstructionTerm();
        actualLine.erase();
        actualToken = this.nextTerm();
        int i = 0;
        int LastILine = 0;
        boolean labelFag;
        while (actualToken.name != EnumToken.EOF) {
            labelFag = false;
            try {
                if (null != actualToken.name) {
                    switch (actualToken.name) {
                        case INSTRUCTION:
                            if (LastILine == line) {
                                outPutErrors += ("Warning: in line " + line + ". More than one Instruction in the line.") + '\n';
                            }
                            LastILine = line;
                            actualLine.setTermA(new Token(actualToken));
                            actualToken = this.nextTerm();
                            if (actualToken.name == EnumToken.REGISTER) {
                                actualLine.setTermB(new Token(actualToken));
                                actualToken = this.nextTerm();
                            } else {
                                throw new java.lang.Exception("LETRAS");
                            }
                            if (actualToken.name == EnumToken.COMMA) {
                                actualToken = this.nextTerm();
                            } else {
                                outPutErrors += ("Warning: line: " + line + ".Missing comma.") + '\n';
                            }
                            if (actualToken.name == EnumToken.REGISTER) {
                                actualLine.setTermC(new Token(actualToken));
                                actualToken = this.nextTerm();
                            } else {
                                throw new java.lang.Exception("LETRAS");
                            }
                            if (actualToken.name == EnumToken.COMMA) {
                                actualToken = this.nextTerm();
                            } else {
                                outPutErrors += ("Warning: line: " + line + ".Missing comma.") + '\n';
                            }
                            if (actualToken.name == EnumToken.REGISTER) {
                                actualLine.setTermD(new Token(actualToken));
                                actualToken = this.nextTerm();
                            } else {
                                throw new java.lang.Exception("LETRAS");
                            }
                            break;
                        case INSTRUCTIONI:
                            if (LastILine == line) {
                                outPutErrors += ("Warning: in line " + line + ". More than one Instruction in the line.") + '\n';
                            }
                            LastILine = line;
                            actualLine.setTermA(new Token(actualToken));
                            if (!"NOP".equals(actualToken.value.toUpperCase())) {
                                actualToken = this.nextTerm();
                                if (actualToken.name == EnumToken.REGISTER) {
                                    actualLine.setTermB(new Token(actualToken));
                                    actualToken = this.nextTerm();
                                } else {
                                    throw new java.lang.Exception("Esperado registrador, encontrado: " + actualToken.value);
                                }
                                if ("PUSH".equals(actualLine.getTermA().value.toUpperCase())
                                        || "POP".equals(actualLine.getTermA().value.toUpperCase())) {
                                    break;
                                } else if (actualToken.name == EnumToken.COMMA) {
                                    actualToken = this.nextTerm();
                                    if (actualLine.getTermA().value.toUpperCase().equals("MOV")) {
                                        if (actualToken.name == EnumToken.REGISTER) {
                                            actualLine.setTermC(new Token(actualToken));
                                            actualToken = this.nextTerm();
                                        } else {
                                            throw new java.lang.Exception("MOV errado.");
                                        }
                                    } else if (actualToken.name == EnumToken.NUMBER
                                            || actualToken.name == EnumToken.ID) {
                                        actualLine.setTermC(new Token(actualToken));
                                        actualToken = this.nextTerm();
                                    } else {
                                        throw new java.lang.Exception();
                                    }
                                } else {
                                    outPutErrors += ("Warning: line: " + line + ".Missing comma.") + '\n';
                                }
                            } else {
                                actualToken = this.nextTerm();
                            }
                            break;
                        case LABEL:
                            System.out.println(actualToken.value + " " + line);
                            labelFag = true;
                            if (LastILine == line) {
                                outPutErrors += ("Warning: in line " + line + ". More than one Instruction in the line.") + '\n';
                            }
                            if (this.containsLabelInVector(actualToken.value) < 0) {
                                LabelTable.add(new LabelNode(FinalTable.size() + 1, actualToken.value, false, line));
                                actualToken = this.nextTerm();
                                actualToken = this.nextTerm();
                            } else {
                                throw new java.lang.Exception("The Label " + actualToken.value + " already defined.");
                            }
                            break;
                        default:
                            throw new java.lang.Exception("Operador invalido: " + actualToken.value + " - " + actualToken.name);
                    }
                }
            } catch (java.lang.Exception e) {
                error = true;
                int errorline = line;
                String errorString = "" + e;

                if (!"LETRAS".equals(errorString)) {
                    outPutErrors += ("Erro na compilacao, line: " + line + ". " + e + "") + '\n';
                } else {
                    outPutErrors += ("Erro na compilacao, line: " + line + ". Registrador invalido. ") + actualToken.value + '\n';
                }

                while (actualToken.name != EnumToken.EOF && line == errorline) {
                    actualToken = this.nextTerm();
                }
                if (actualToken.name == EnumToken.EOF) {
                    break;
                }
            }
            if (!labelFag) {
                FinalTable.add(new InstructionTerm(actualLine));
            }
            actualLine.erase();
            i++;
        }
    }

    public List<String> writeStream() {
        List<String> outStream = new ArrayList<>();
        int size = FinalTable.size();
        int i = 0;

        String A, B, C, D = "";

        for (InstructionTerm instructionTerm : FinalTable) {
            B = C = D = "";
            A = instructionTerm.getTermA().CorrespondenceString();
            if (instructionTerm.getTermA().name == EnumToken.INSTRUCTION) {
                B = instructionTerm.getTermC().CorrespondenceString();
                C = instructionTerm.getTermD().CorrespondenceString();
                D = instructionTerm.getTermB().CorrespondenceString();
            } else if (instructionTerm.getSize() > 1) {
                if (instructionTerm.getTermA().value.toUpperCase().equals("MOV")) {
                    B = "00" + instructionTerm.getTermC().CorrespondenceString();
                    C = instructionTerm.getTermB().CorrespondenceString();
                } else if (instructionTerm.getTermA().value.toUpperCase().equals("PUSH")) {
                    B = "00" + instructionTerm.getTermB().CorrespondenceString();
                    C = "00";
                } else if (instructionTerm.getTermA().value.toUpperCase().equals("POP")) {
                    B = "0000";
                    C = instructionTerm.getTermB().CorrespondenceString();
                } else {
                    B = instructionTerm.getTermB().CorrespondenceString();
                    C = instructionTerm.getTermC().CorrespondenceString();
                }
            }
            outStream.add(A + B + C + D + '\n');
            i++;
        }
        return outStream;
    }

    public void writeFile(List<String> outStream, String out, boolean VhdlFlag) throws IOException {
        File outputFile = new File(out);
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        String outStreamAux = new String();
        if (!VhdlFlag) {
            outStreamAux = Constantes.VHDL_FILE_PART1;
            int i = 15;
            for (String string : outStream) {
                outStreamAux += toVhdlInstruction(string, i++);
            }
            outStreamAux += "			others => (0,0,0,0,0,0,0,0));\n";
            outStreamAux += "                   program_size <= " + programSize(i+1) + ";\n";
            outStreamAux += Constantes.VHDL_FILE_PART2;
        }
        bw.write(outStreamAux);
        bw.close();
    }

    private String toVhdlInstruction(String string, int i) {
        String aux = "			" + (i) + " => ("
                + string.charAt(0) + ","
                + string.charAt(1) + ", "
                + string.charAt(2) + ","
                + string.charAt(3) + ", "
                + string.charAt(4) + ","
                + string.charAt(5) + ", "
                + string.charAt(6) + ","
                + string.charAt(7) + "),\n";
        return aux;
    }

    private String programSize(int i) {
        String retString = Integer.toString(i, 4);

        while (retString.length() < 4) {
            retString = '0' + retString;
        }

        retString = "(" + retString.charAt(0) + ','
                + retString.charAt(1) + ','
                + retString.charAt(2) + ','
                + retString.charAt(3)
                + ')';

        return retString;
    }
}
