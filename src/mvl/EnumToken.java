package mvl;

public enum EnumToken {

    //instructions
    INSTRUCTIONI,
    STORE,
    JUMP,
    ADDI,
    NOP,
    LOAD,
    INSTRUCTION,
    ADD,
    SUBT,
    MULT,
    MOV,
    DIV,
    PUSH,
    GT,
    LS,
    
    //Registers
    REGISTER,
    
    //NumTypes
    INTEGER,
    HEXNUM,
    
    //Constants
    EOF,
    UNDEF_INICIAL,
    NUMBER,
    ID,
    UNDEF,
    COMMA,
    LABEL
}
