package mvl;

public class Exception extends RuntimeException
{
    private final String msg;

    public Exception() 
    {       
        msg = "Unexpected";
    }
    
    public Exception(String str)
    {
        super(str);
        msg = str ;
    }
    
    public Exception(String str, int inLine)
    {
        super(str);
        msg = "Erro em linha " + inLine + ": "+ str ;
    }
    
    public String toString()
    {
        return msg;
    }
}
