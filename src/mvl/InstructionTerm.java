package mvl;

import lombok.Getter;

@Getter
public class InstructionTerm {

    private Token termA;
    private Token termB;
    private Token termC;
    private Token termD;
    private int size;

    public int getSize() {
        return size;
    }

    InstructionTerm(InstructionTerm inTer) {
        this.termA = inTer.termA;
        this.termC = inTer.termC;
        this.termD = inTer.termD;
        this.termB = inTer.termB;
        this.size = inTer.size;
    }

    InstructionTerm( final Token A, Token B, Token C, Token D ){
        super();
        if (A != null) this.setTermA(A);
        if (B != null) this.setTermB(B);
        if (C != null) this.setTermC(C);
        if (D != null) this.setTermD(D);
    }
    
    public InstructionTerm() {
        this.termA = null;
        this.termB = null;
        this.termC = null;
        this.termD = null;
        size = 0;
    }

    public void erase() {
        this.termA = null;
        this.termB = null;
        this.termC = null;
        this.termD = null;
        size = 0;
    }

    public void setTermD(Token TermD) {
        this.size++;
        this.termD = TermD;
    }

    public void setTermA(Token TermA) {
        this.size++;
        this.termA = TermA;
    }

    public void setTermB(Token TermB) {
        this.size++;
        this.termB = TermB;
    }

    public void setTermC(Token TermC) {
        this.size++;
        this.termC = TermC;
    }
}
