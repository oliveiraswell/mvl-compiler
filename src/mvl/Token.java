package mvl;

class Token {

    public EnumToken name;
    public EnumToken attribute;
    public String value;

    public Token(EnumToken name) {
        this.name = name;
        attribute = EnumToken.UNDEF;
    }

    public Token(Token inTok) {
        this.name = inTok.name;
        this.attribute = inTok.attribute;
        this.value = inTok.value;
    }

    public Token(EnumToken name, EnumToken attr) {
        this.name = name;
        attribute = attr;
    }

    public Token(EnumToken name, String value) {
        this.name = name;
        this.value = value;
    }

    public String CorrespondenceString() {
        String ret = "";
        Integer aux;

        if (this.name == EnumToken.INSTRUCTION
                || this.name == EnumToken.INSTRUCTIONI) {
            switch (this.value.toUpperCase()) {
                case "NOP":
                    ret = "00000000";
                    break;
                case "LOAD":
                    ret = "23";
                    break;
                case "ADDI":
                    ret = "20";
                    break;
                case "ADD":
                    ret = "10";
                    break;
                case "SUBT":
                    ret = "11";
                    break;
                case "MULT":
                    ret = "12";
                    break;
                case "MOV":
                    ret = "13";
                    break;
                case "JUMP":
                    ret = "01";
                    break;
                case "STORE":
                    ret = "33";
                    break;
                case "DIV":
                    ret = "21";
                    break;
                case "PUSH":
                    ret = "31";
                    break;
                case "POP":
                    ret = "32";
                    break;
                case "GT":
                    ret = "22";
                    break;
                case "LS":
                    ret = "30";
                    break;    
                default:
                    break;
            }
        } else if (this.name == EnumToken.REGISTER) {
            aux = Integer.parseInt(this.value);
            ret = Integer.toString(aux, 4);
            while (ret.length() < 2) {
                ret = '0' + ret;
            }
        } else if (this.name == EnumToken.NUMBER) {
            aux = Integer.parseInt(this.value);
            if (aux >= 0) {
                ret = Integer.toString(aux, 4);
            } else {
                ret = "3333";
            }
            while (ret.length() < 4) {
                ret = '0' + ret;
            }
        }
        return ret;
    }
}
