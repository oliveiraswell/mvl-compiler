            ADDI $8, 8
            ADDI $4, 0

            ADDI $0, 1
            ADDI $1, 1
            PUSH $0
            PUSH $1

FIBONACCI:
            ADD   $2, $1, $0 -- Reg 2 destino
            PUSH  $2

            MOV $0, $1
            MOV $1, $2
            
            ADDI  $8, -1
            JUMP  $8, END

            JUMP  $4, FIBONACCI

END:
	    POP $10
            POP $9
            POP $8
            POP $7
            POP $6
            POP $5
            POP $4
            POP $3
            POP $2
            POP $1
            POP $0
